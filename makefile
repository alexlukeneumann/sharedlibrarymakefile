#CONFIGURATION
DIRECTORIES = Shared Example
BIN_DIR = ./bin
EXECUTABLE_NAME = Example.exe

.PHONY: all clean

all: build run

build:
	for dir in $(DIRECTORIES) ; do 		\
		cd $$dir && make all && cd ../ ; done

clean:
	for dir in $(DIRECTORIES) ; do 		\
		cd $$dir && make $@ && cd ../ ; done

run: 
	$(BIN_DIR)/${EXECUTABLE_NAME}
	