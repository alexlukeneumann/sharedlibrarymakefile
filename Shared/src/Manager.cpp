#include "Manager.h"

namespace shared 
{
    Manager::Manager(const std::string& data)
    {
        std::cout << "This is a manager: " << data << std::endl;
    }
}