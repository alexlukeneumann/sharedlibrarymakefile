#pragma once

#ifdef SHARED_EXPORTS
#define API __declspec(dllexport)
#else
#define API __declspec(dllimport)
#endif

#include <iostream>
#include <string>

namespace shared
{
    class API Manager
    {
    public:
        Manager(const std::string& data);
    };
}