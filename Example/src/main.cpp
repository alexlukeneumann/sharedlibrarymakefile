#include <iostream>

#include "includes/functions.h"

#include <Manager.h>

int main()
{
    std::cout << "Hello world!" << std::endl;

    std::cout << "Inside the includes folder: " << Functions::Add(5.5, 3.2) << std::endl;

    shared::Manager manager("This is from the shared library");

    return 0;
}
